<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-generic-unified-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Unified;

use Stringable;

/**
 * UnifiedObjectInterface interface file.
 * 
 * This interface is not meant to be implemented directly. This interface is
 * a superinterface from which all Unifiedxxx sub-interfaces must extend in
 * order to have generic informations and behavior.
 * 
 * Every information carried by those objects are not mandatory, depending on
 * the available information of the source of information under the
 * implementation of those interfaces.
 * 
 * The mandatory informations are :
 * - Some identifier of the source of this object
 * - Some identifier for this object specifically
 * - The name of this object is some language (usually english)
 * 
 * @author Anastaszor
 */
interface UnifiedObjectInterface extends Stringable
{
	
	/**
	 * Gets a string that uniquely identifies this object's source. The source
	 * is an unique identifier for the implementation's library. This should not
	 * be no longer that a few (3-4) characters.
	 * 
	 * @return string
	 */
	public function getSourceId() : string;
	
	/**
	 * Gets a string that uniquely identifies this object into the information
	 * system of the object. Equality of this value amongst two unified objects
	 * of the same class means they represent the same object. This identifier
	 * should be an ASCII string.
	 * 
	 * @return string
	 */
	public function getUniqueId() : string;
	
	/**
	 * Gets the name of this object in some language. This object should be
	 * able to determine an output string based on the data it holds. Names
	 * should be given in utf-8 encoding.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
}
