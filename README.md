# php-extended/php-generic-unified-interface
A generic library to build unified interfaces

![coverage](https://gitlab.com/php-extended/php-generic-unified-interface/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-extended/php-generic-unified-interface ^8`


## Basic Usage

This library is an interface only library.

This library does not have direct implementations, but some other interface
libraries depends on it.


## License

MIT (See [license file](LICENSE)).
